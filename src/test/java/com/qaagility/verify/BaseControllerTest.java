package com.qaagility.verify;

import static org.junit.Assert.*;
import java.io.*;
import javax.servlet.http.*;
import org.junit.Test;
import org.mockito.Mockito;
import com.qaagility.controller.*;
 
import org.junit.Before;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class BaseControllerTest {
    @InjectMocks
    private BaseController baseController;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(baseController).build();
    }
    @Test
    public void testCreate() throws Exception {
        this.mockMvc.perform(get("/")).andExpect(status().isOk());
    }
    @Test
    public void testCreateName() throws Exception {
        this.mockMvc.perform(get("/Vaishali")).andExpect(status().isOk());

    }


    @Test
    public void testAdd() throws Exception {

        int k= new Calculator().add();
        assertEquals("Problem with Add function:", 9, k);
        
    }

    @Test
    public void testMul() throws Exception {

        int k= new Calcmul().mul();
        assertEquals("Problem with Sub function:", 18, k);

    }

    @Test
    public void testd() throws Exception {

        int k= new Cnt().d(6,2);
        assertEquals("Problem with d function",3,k);
}

    @Test
    public void testd1() throws Exception {
         int k = new Cnt().d(6,0);
         assertEquals("Problem with d function",Integer.MAX_VALUE,k);

    }
}


}
